# 服务注册中心

运行启动类：com.kdyzm.spring.security.oauth.study.register.server.RegisterServer

之后打开网址 [http://127.0.0.1:8765](http://127.0.0.1:8765)，出现以下页面表示启动成功

![eureka-server](../git-imgs/eureka server.png)
